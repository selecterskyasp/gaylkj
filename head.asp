﻿<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/css.css" type="text/css" media="all" /> 
<meta name="robots" content="all" /> 
<meta http-equiv="imagetoolbar" content="false" /> 
<meta name="MSSmartTagsPreventParsing" content="true" /> 
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" /> 
<meta name="Copyright" content="Copyright (c) 2009 www.changsheng8.com" /> 
<script language="javascript" src="js/function.js"></script>
<meta name="Keywords" content="$pageKeywords$" /> 
<meta name="Description" content="$pageDescription$" /> 
<title>$pagetitle$</title>
</head>

<body>
<table width="980" height="97" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="77"><img src="images/top1.gif" width="77" height="97" /></td>
    <td width="80"><img src="images/toplogo.gif" width="80" height="96" /></td>
    <td width="588"><img src="images/topbanner.gif" width="588" height="96" /></td>
    <td width="234" valign="top"><table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="100%" height="53"><img src="images/top2.gif" width="234" height="53" /></td>
      </tr>
      <tr>
        <td height="21"><table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="12"><img src="images/topfavleft.gif" width="12" height="21" /></td>
            <td align="center" background="images/topfavbg.gif"><a href="default.asp" class="blue">英文版</a> | <a href="javascript:;" class="blue" onClick="cjx.setHome(this,'$cityurl$')">设为首页</a> | <a href="javascript:;" class="blue" onClick="cjx.myAddBookmark('$cityname$','$cityurl$');">加入收藏</a></td>
            <td width="9"><img src="images/topfavright.gif" width="9" height="22" /></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
<table width="980" height="16" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="images/tline1.gif" width="980" height="15" /></td>
  </tr>
</table>
<table width="980" height="239" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="48"><img src="images/mid1.gif" width="48" height="239" /></td>
    <td width="237"><table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="50%" height="28" align="right"><h2 class="blue"><img src="images/jiahao.gif" width="9" height="8"/>&nbsp;<a href="index.html">网站首页</a></h2></td>
        <td width="50%" align="left"><span class="eng">HOME</span></td>
      </tr>
       <tr>
        <td width="50%" height="28" align="right"><h2 class="blue"><img src="images/jiahao.gif" width="9" height="8"/>&nbsp;<a href="index.html">公司介绍</a></h2></td>
        <td width="50%" align="left"><span class="eng">INTRODUCTION</span></td>
      </tr>
      <tr>
        <td width="50%" height="28" align="right"><h2 class="blue"><img src="images/jiahao.gif" width="9" height="8"/>&nbsp;<a href="news.asp">新闻中心</a></h2></td>
        <td width="50%" align="left"><span class="eng">NEWS</span></td>
      </tr>
      <tr>
        <td width="50%" height="28" align="right"><h2 class="blue"><img src="images/jiahao.gif" width="9" height="8"/>&nbsp;<a href="products.asp">产品展示</a></h2></td>
        <td width="50%" align="left"><span class="eng">PRODUCTS</span></td>
      </tr>
      <tr>
        <td width="50%" height="28" align="right"><h2 class="blue"><img src="images/jiahao.gif" width="9" height="8"/>&nbsp;<a href="jishu.asp">技术资料</a></h2></td>
        <td width="50%" align="left"><span class="eng">TECHNICAL</span></td>
      </tr>
      <tr>
        <td width="50%" height="28" align="right"><h2 class="blue"><img src="images/jiahao.gif" width="9" height="8"/>&nbsp;<a href="order.asp">在线订购</a></h2></td>
        <td width="50%" align="left"><span class="eng">ONLINE BUY</span></td>
      </tr>
      <tr>
        <td width="50%" height="28" align="right"><h2 class="blue"><img src="images/jiahao.gif" width="9" height="8"/>&nbsp;<a href="index.html">在线留言</a></h2></td>
        <td width="50%" align="left"><span class="eng">MESSAGE</span></td>
      </tr>
      <tr>
        <td width="50%" height="28" align="right"><h2 class="blue"><img src="images/jiahao.gif" width="9" height="8"/>&nbsp;<a href="index.html">联系我们</a></h2></td>
        <td width="50%" align="left"><span class="eng">CONTACT</span></td>
      </tr>
    </table></td>
    <td width="695"><img src="images/banner.gif" width="695" height="239" /></td>
  </tr>
</table>
<table width="980" height="16" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="images/tlline2.gif" width="980" height="16" /></td>
  </tr>
</table>
<table width="980" height="37" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>