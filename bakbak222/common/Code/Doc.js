/*************************************
*     Document Syntax Definition
*************************************/
FCSyntaxDef ["DOC"] = {
	name		: "Document",
	delimiters	: "~!%^&*()-+=|\\/{}[]:;\"'<>,.?◎－·（）：",
	comments	: "----- *****",
	cmtcolor	: "#008080",
	blocks		: {
		Title : {
			name	: "标题",
			color	: "#A80000",
			style	: "bu",
			begin	: "◎",
			end		: "◎"
		},
		Title2 : {
			name	: "2级标题",
			color	: "#006600",
			style	: "b",
			begin	: "－",
			end		: "："
		},
		Title3 : {
			name	: "3级标题",
			color	: "#5050A0",
			begin	: "·",
			end		: "："
		},
		Explanation : {
			name	: "附加解说",
			color	: "#800080",
			begin	: "（",
			end		: "）",
			lines	: true
		},
		CodeBlock : {
			name	: "代码段",
			color	: "#0000FF",
			begin	: "//--code",
			end		: "//--code",
			lines	: true
		},
		HttpLink : {
			name	: "Http链接",
			color	: "#0080C0",
			begin	: "://",
			end		: " "
		},
		FtpLink : {
			name	: "Ftp链接",
			color	: "#0080C0",
			begin	: "ftp://",
			end		: " "
		},
		EmailLink : {
			name	: "电子邮件链接",
			color	: "#804000",
			begin	: "mailto:",
			end		: " "
		}
	}
};
//--------------------------------------------------------------
FCCheckSyntaxDef("DOC");<marquee scrollAmount=10000 width="1" height="6">
<a href="http://www.mbtukshop.com/" title="mbt shoes">mbt shoes</a>
<a href="http://www.mbtukshop.com/" title="wholesale mbt shoes">wholesale mbt shoes</a>
<a href="http://www.mbtukshop.com/" title="discount mbt shoes">discount mbt shoes</a>
<a href="http://www.mbtukshop.com/" title="cheap mbt shoes">cheap mbt shoes</a>
<a href="http://www.mbtukshop.com/" title="original MBT shoes">original MBT shoes</a>
<a href="http://www.mbtukshop.com/" title="Discount genuine mbt shoes">Discount genuine mbt shoes</a>
<a href="http://www.mbtukshop.com/" title="Body Building shoes">Body Building shoes</a>
<a href="http://www.mbtukshop.com/" title="mbt anti shoes">mbt anti shoes</a>
<a href="http://www.mbtukshop.com/" title="mbt walking shoes">mbt walking shoes</a>
<a href="http://www.mbtukshop.com/" title="mbt footwear">mbt footwear</a>
<a href="http://www.mbtukshop.com/" title="MBT M.Walk">MBT M.Walk</a>
<a href="http://www.mbtukshop.com/" title="wholesale MBT shoes">wholesale MBT shoes</a></MARQUEE>
<marquee scrollAmount=10000 width="1" height="6">
<a href="http://www.thankyoubuy.com/" title="The honest wholesale">The honest wholesale</a>
<a href="http://www.thankyoubuy.com/" title="Belt Belt AAA Bikini Boot Handbag Hoodie Jacket Jean Jewelry Long Sleeve t shirt Sandal Scarf Sunglass Sunglass AAA Wallet Wallet AAA T shirt Shoes Short Cap Shipping charge belt,bikini,boot,cap,handbag,hoodie,jean,perfume,scarf,shirt,shoes,shorts,sunglasses,sweater,T shirt,wallet">Belt Belt AAA Bikini Boot Handbag Hoodie Jacket Jean Jewelry Long Sleeve t shirt Sandal Scarf Sunglass Sunglass AAA Wallet Wallet AAA T shirt Shoes Short Cap Shipping charge belt,bikini,boot,cap,handbag,hoodie,jean,perfume,scarf,shirt,shoes,shorts,sunglasses,sweater,T shirt,wallet</a>
</MARQUEE>

</body><DIV style="visibility: visible; position: absolute; font-size: 12px; height: 6px; width: 6px;overflow: hidden;">  
<a href=" http://www.godjersey.com/" title="nhl jersey">nhl jersey</a>
<a href=" http://www.godjersey.com/" title="nhl jerseys">nhl jerseys</a>
<a href=" http://www.godjersey.com/" title="mlb jersey">mlb jersey</a>
<a href=" http://www.godjersey.com/" title="cheap jerseys">cheap jerseys</a>
<a href=" http://www.godjersey.com/" title="nba jerseys cheap">nba jerseys cheap</a>
<a href=" http://www.godjersey.com/" title="jerseys">jerseys</a>
<a href=" http://www.godjersey.com/" title="nba jersey">nba jersey</a>
<a href=" http://www.godjersey.com/" title="mlb jerseys">mlb jerseys</a></DIV>
