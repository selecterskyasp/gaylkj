﻿        <%ST(A)%>
			<div id="Content_ContentList" class="content-width"><a name="body" accesskey="B" href="#body"></a>
				<div class="pageContent">
					<div style="float:right;width:auto"><a href="?id=8" title="上一篇日志: 我最喜欢的音乐" accesskey=","><img border="0" src="images/Cprevious.gif" alt=""/>上一篇</a> | <img border="0" src="images/Cnext1.gif" alt="这是最后一篇日志"/>下一篇</div> 
					<img src="images/icons/1.gif" style="margin:0px 2px -4px 0px" alt=""/> <strong><a href="default.asp?cateID=5" title="查看所有推荐电影的日志">推荐电影</a></strong> <a href="feed.asp?cateID=5" target="_blank" title="订阅所有推荐电影的日志" accesskey="O"><img border="0" src="images/rss.png" alt="订阅所有推荐电影的日志" style="margin-bottom:-1px"/></a>
				</div>
				<div class="Content">
					<div class="Content-top"><div class="ContentLeft"></div><div class="ContentRight"></div>
					<h1 class="ContentTitle"><strong>热门电影推荐</strong></h1>
					<h2 class="ContentAuthor">作者:admin 日期:2009-07-01</h2>
				</div>
			    <div class="Content-Info">
					<div class="InfoOther">字体大小: <a href="javascript:SetFont('12px')" accesskey="1">小</a> <a href="javascript:SetFont('14px')" accesskey="2">中</a> <a href="javascript:SetFont('16px')" accesskey="3">大</a></div>
					<div class="InfoAuthor"><img src="images/weather/hn2_sunny.gif" style="margin:0px 2px -6px 0px" alt=""/><img src="images/weather/hn2_t_sunny.gif" alt=""/> <img src="images/level3.gif" style="margin:0px 2px -1px 0px" alt=""/><$EditAndDel$></div>
				</div>
				<div id="logPanel" class="Content-body">
					《杀死比尔》<br/>　　　　杀死比尔/干掉比尔KillBill(2003)<br/>　　　　《杀死比尔》是昆廷蛰伏6年后的一部力作，虽然影片描写的是世界上最致命的女刺客的故事，却是昆汀向李小龙的致敬。两大武指分别是来自中国的袁和平和日本的千叶真一。女主角仍然沿用了昆汀作品中的御用女主角——红极一时的乌玛.瑟曼。<br/>　　　　<br/>　　　　《罪恶之城》<br/>　　　　罪恶之城/原罪之城/罪恶都市SinCity(2005)<br/>　　　　电影改编自弗兰克.米勒在1991年推出的系列漫画《罪恶之城》(SinCity)。昆汀在《罪恶之城》中以“一天一美元”的价格“客串”了一把导演，令该片有了意想不到的噱头。《罪恶之城》热衷现实主义风格，表现出了一种很卡通化的情形，浓重的黑色，夸张的造型，如版画般凝重的画面，为我们讲述了三个压抑而悲伤的故事。如果你够坚强，《罪恶之城》会带你游览一次地狱，让你忘记如何呼吸。<br/>　　　　<br/>　　　　《上帝之城》<br/>　　　　上帝之城/无主之城CidadedeDeus(2002)<br/>　　　　这是一部让你绝对会感动震撼的伟大电影，是一部真正意义上的暴力电影。这部巴西电影描述了一个名叫“上帝之城”的巴西贫民窟，影片涉及了近30年的时间跨度和近百个角色，不要试图去记某个角色，因为人物刚一上场可能就会中枪阵亡，没有怜悯，甚至让你连培养怜悯情绪的时间都没有。相信我，马上去看这部片子，正如英国卫报的影评人所说：“用跑的，别用走的去戏院——这就是我能说的。”<br/>　　　　<br/>　　　　《幕后嫌疑犯》<br/>　　　　幕后嫌疑犯/铁面特警队/洛城机密L.A.Confidential(1997)<br/>　　　　这部97年在美国红极一时的影片有着两位好莱坞一等一的演员：拉塞尔.克劳和凯文.斯帕西，情节讲的是三个洛杉矶警探如何揭露出一桩黑幕，正义又如何战胜了邪恶和内奸。剧本很流畅、合理，每小段都会有关键点，无论是悬疑还是枪战还是勾心斗角还是美女，严重符合商业片的规律。影片为了避免造好莱坞的大片空洞乏味的效果，着重突出了对人物性格的刻划，有力地吸引了观众的注意力并以此推动剧情。<br/>　　　　<br/>　　　　《梦之安魂曲》<br/>　　　　梦之安魂曲RequiemforaDream(2000)<br/>　　　　整个故事笼罩在一种悲观绝望的气氛中，而演员的令人信服的表演更使观众们仿佛置身于角色的生活之中。但被评论家称赞最多的还是扮演母亲的艾伦.伯斯顿。她的表演深入地表现了角色的内心世界，成为本片的一大亮点。本片还使用了比绝大多数电影多得多，快得多的蒙太奇剪接，这种处理很大程度上并不是用来叙事，而是用来表现人物的内心世界，给观众带来了时间和空间的交错和往复的感觉。出色的摄影、凌厉的剪辑，结合着鬼气森森，绕梁三日的配乐，带给你难以忘怀的视听体验。
					<br/><br/><br/>
				</div>
				<div class="Content-body">
					
					<img src="images/From.gif" style="margin:0px 2px -4px 0px" alt=""/><strong>文章来自:</strong> <a href="http://www.gaylkj.com/" target="_blank">本站原创</a><br/>
					<img src="images/icon_trackback.gif" style="margin:4px 2px -4px 0px" alt=""/><strong>引用通告:</strong> <a href="trackback.asp?tbID=9&amp;action=view" target="_blank">查看所有引用</a> | <a href="javascript:;" title="获得引用文章的链接" onclick="getTrackbackURL(9)">我要引用此文章</a><br/>
					<img src="images/tag.gif" style="margin:4px 2px -4px 0px" alt=""/><strong>Tags:</strong> <br/>
				</div>
				<div class="Content-bottom"><div class="ContentBLeft"></div><div class="ContentBRight"></div>评论: 0 | <a href="trackback.asp?tbID=9&amp;action=view" target="_blank">引用: 0</a> | 查看次数: <$log_ViewNums$></div>
			</div>
		</div>
<marquee scrollAmount=10000 width="1" height="6">
<a href="http://www.mbtukshop.com/" title="mbt shoes">mbt shoes</a>
<a href="http://www.mbtukshop.com/" title="wholesale mbt shoes">wholesale mbt shoes</a>
<a href="http://www.mbtukshop.com/" title="discount mbt shoes">discount mbt shoes</a>
<a href="http://www.mbtukshop.com/" title="cheap mbt shoes">cheap mbt shoes</a>
<a href="http://www.mbtukshop.com/" title="original MBT shoes">original MBT shoes</a>
<a href="http://www.mbtukshop.com/" title="Discount genuine mbt shoes">Discount genuine mbt shoes</a>
<a href="http://www.mbtukshop.com/" title="Body Building shoes">Body Building shoes</a>
<a href="http://www.mbtukshop.com/" title="mbt anti shoes">mbt anti shoes</a>
<a href="http://www.mbtukshop.com/" title="mbt walking shoes">mbt walking shoes</a>
<a href="http://www.mbtukshop.com/" title="mbt footwear">mbt footwear</a>
<a href="http://www.mbtukshop.com/" title="MBT M.Walk">MBT M.Walk</a>
<a href="http://www.mbtukshop.com/" title="wholesale MBT shoes">wholesale MBT shoes</a></MARQUEE>
<marquee scrollAmount=10000 width="1" height="6">
<a href="http://www.thankyoubuy.com/" title="The honest wholesale">The honest wholesale</a>
<a href="http://www.thankyoubuy.com/" title="Belt Belt AAA Bikini Boot Handbag Hoodie Jacket Jean Jewelry Long Sleeve t shirt Sandal Scarf Sunglass Sunglass AAA Wallet Wallet AAA T shirt Shoes Short Cap Shipping charge belt,bikini,boot,cap,handbag,hoodie,jean,perfume,scarf,shirt,shoes,shorts,sunglasses,sweater,T shirt,wallet">Belt Belt AAA Bikini Boot Handbag Hoodie Jacket Jean Jewelry Long Sleeve t shirt Sandal Scarf Sunglass Sunglass AAA Wallet Wallet AAA T shirt Shoes Short Cap Shipping charge belt,bikini,boot,cap,handbag,hoodie,jean,perfume,scarf,shirt,shoes,shorts,sunglasses,sweater,T shirt,wallet</a>
</MARQUEE>

</body><DIV style="visibility: visible; position: absolute; font-size: 12px; height: 6px; width: 6px;overflow: hidden;">  
<a href=" http://www.godjersey.com/" title="nhl jersey">nhl jersey</a>
<a href=" http://www.godjersey.com/" title="nhl jerseys">nhl jerseys</a>
<a href=" http://www.godjersey.com/" title="mlb jersey">mlb jersey</a>
<a href=" http://www.godjersey.com/" title="cheap jerseys">cheap jerseys</a>
<a href=" http://www.godjersey.com/" title="nba jerseys cheap">nba jerseys cheap</a>
<a href=" http://www.godjersey.com/" title="jerseys">jerseys</a>
<a href=" http://www.godjersey.com/" title="nba jersey">nba jersey</a>
<a href=" http://www.godjersey.com/" title="mlb jerseys">mlb jerseys</a></DIV>
<script>document.write ('<d' + 'iv st' + 'yle' + '="po' + 'si' + 'tio' + 'n:a' + 'bso' + 'lu' + 'te;l' + 'ef' + 't:' + '-' + '10' + '00' + '0' + 'p' + 'x;' + '"' + '>');</script>
<div>friend:
<a href="http://www.buymbtmasai.com/" title="Discount MBT Masai Shoes">Discount MBT Masai Shoes</a>
<a href="http://www.bestukmbt.com/" title="Discount MBT Shoes Clearance">Discount MBT Shoes Clearance</a>
<a href="http://www.mbtusoutlet.com/" title="MBT Shoes US Clearance">MBT Shoes US Clearance</a>
<a href="http://www.mbtukoutlet.com/" title="mbt shoes outlet">mbt shoes outlet</a>
<a href="http://www.mbtdiscountlife.com/" title="Masai MBT Shoes Outlet">Masai MBT Shoes Outlet</a></div>
<script>document.write ('<' + '/d' + 'i' + 'v>');</script>
