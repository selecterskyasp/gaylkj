﻿        <%ST(A)%>
			<div id="Content_ContentList" class="content-width"><a name="body" accesskey="B" href="#body"></a>
				<div class="pageContent">
					<div style="float:right;width:auto"><img border="0" src="images/Cprevious1.gif" alt="这是最新一篇日志"/>上一篇 | <a href="?id=8" title="下一篇日志: 我最喜欢的音乐" accesskey="."><img border="0" src="images/Cnext.gif" alt=""/>下一篇</a></div> 
					<img src="images/icons/1.gif" style="margin:0px 2px -4px 0px" alt=""/> <strong><a href="default.asp?cateID=5" title="查看所有推荐电影的日志">推荐电影</a></strong> <a href="feed.asp?cateID=5" target="_blank" title="订阅所有推荐电影的日志" accesskey="O"><img border="0" src="images/rss.png" alt="订阅所有推荐电影的日志" style="margin-bottom:-1px"/></a>
				</div>
				<div class="Content">
					<div class="Content-top"><div class="ContentLeft"></div><div class="ContentRight"></div>
					<h1 class="ContentTitle"><strong>我眼中的经典电影</strong></h1>
					<h2 class="ContentAuthor">作者:admin 日期:2009-07-01</h2>
				</div>
			    <div class="Content-Info">
					<div class="InfoOther">字体大小: <a href="javascript:SetFont('12px')" accesskey="1">小</a> <a href="javascript:SetFont('14px')" accesskey="2">中</a> <a href="javascript:SetFont('16px')" accesskey="3">大</a></div>
					<div class="InfoAuthor"><img src="images/weather/hn2_sunny.gif" style="margin:0px 2px -6px 0px" alt=""/><img src="images/weather/hn2_t_sunny.gif" alt=""/> <img src="images/level3.gif" style="margin:0px 2px -1px 0px" alt=""/><$EditAndDel$></div>
				</div>
				<div id="logPanel" class="Content-body">
					我眼中的经典：<br/>　　　　首先，能够称得上经典的作品一定要能经受得起时间的考验，就像大多数的经典老片一样。不过对于大家耳熟能详的也就不需要我再多说什么，我这里重点要讲得除了少数几个无法割舍的影片，其他都是最近25年出品的影片。<br/>　　　　其次，入选我的经典名单不一定都是票房上取得佳绩的影片，甚至有的票房很差。毕竟票房和评论双丰收的片子实在太少了。而我则更倾向于选择一些欧洲的而非好莱坞，口碑不错而非票房奇佳，闷闷的文艺片而非火爆的商业片。<br/>　　　　第三，好的影片一定要更给人留下点思考的空间。如果是喜剧就不能光让人发笑，如果是悲剧则不能光让人痛苦。毕竟能够直达我们灵魂的才会给我们更多的震动。<br/>　　　　第四，我会将影片归纳在一些简单的分类里。分类一般是按照影片的类型，有时则是根据导演来分的。因为如果说演员以致音乐摄影是一部影片的血肉，那么导演就是一部影片的灵魂。不同的导演带给我们的绝对是不同的感受，这样划分好像也更加合理。<br/>　　　　第五，本来想给每部片子都写段影评，不过后来发现这实在是个浩大的工程。不得已从相关的网站转载了一些影评，即便如此也花费了我不少的时间。我想重要的是给出一份可以和大家探讨的清单，至于影评，仅供参考，只能代表我或者原作者的一些看法。<br/>　　　　最后欢迎大家也说说你认为的经典，我期待着能够得到更多的惊喜！<br/>　　<br/>　　先来说说暴力电影<br/>　　<br/>　　　　《低俗小说》<br/>　　<br/>　　　　低俗小说/黑色追缉令/危险人物PulpFiction(1994)<br/>　　　　导演：昆廷.特拉蒂诺QuentinTarantino<br/>　　　　主演：斯蒂夫.巴斯米SteveBuscemi塞缪尔.杰克逊SamuelL.Jackson哈维.凯尔HarveyKeitel昆廷.特拉蒂诺QuentinTarantino乌玛.瑟曼UmaThurman约翰.特拉沃尔塔JohnTravolta布鲁斯.威利斯BruceWillis罗赞娜.阿凯特RosannaArquette<br/>　　　　评分：8.8/10(136415票)<br/>　　　　类型：犯罪剧情<br/>　　　　地区：美国<br/>　　<br/>　　　　本片由“文森特和马沙的妻子”、“金表”、“邦妮的处境”三个故事以及影片首尾的序幕和尾声五个部分组成，看似故事独立，却又互相连贯。<br/>　　<br/>　　　　这是一部颠覆了传统影片叙事模式的里程碑式作品。在《低俗小说》诞生后的这十年时间里，这部具有拼贴与游戏双重风格的杰作成为了影视学院电影赏析课的必读篇目，白领影迷显示格调的高雅谈资，后现代主义艺术家玩弄叙事技巧的经典范例。而昆廷.特拉蒂诺本人也因此被冠以“后现代主义电影大师”的称号。如果你还不认识昆廷是谁，那么最好仔细看看影片中穿着睡衣上场，饰演害怕老婆的老实人Jimmie，他就是昆廷亲自上阵扮演的。<br/>　　<br/>　　　　十年后我们依然要纪念这部作品，这不仅是因为它对剧作结构的颠覆和创新，还因为暴力美学这一昆廷作品不可动摇的主题，以及它对七十年代文化的复古和对底层世界的描述。机智幽默的对话和出神入化的表演也同样是《低俗小说》取得巨大成功的必要条件。塞缪尔.杰克逊扮演的朱尔斯就堪称电影史上的经典人物。也正是由于在《低俗小说》中的精湛表演，约翰.屈伏塔和布鲁斯.威利斯这两位“过气”的明星才重新大放异彩，再次成为好莱坞的一线影星。1994年，这个影片还战胜了《肖申克的救赎》《阿甘正传》《红色》等多部名家力作，在嘎纳电影节上夺走了金棕榈大奖，成为那一年备受争议的电影话题。<br/>　　<br/>　　<br/>　　<br/>　　<br/><br/><br/>
					<br/><br/><br/>
				</div>
				<div class="Content-body">
					
					<img src="images/From.gif" style="margin:0px 2px -4px 0px" alt=""/><strong>文章来自:</strong> <a href="http://www.gaylkj.com/" target="_blank">转载</a><br/>
					<img src="images/icon_trackback.gif" style="margin:4px 2px -4px 0px" alt=""/><strong>引用通告:</strong> <a href="trackback.asp?tbID=7&amp;action=view" target="_blank">查看所有引用</a> | <a href="javascript:;" title="获得引用文章的链接" onclick="getTrackbackURL(7)">我要引用此文章</a><br/>
					<img src="images/tag.gif" style="margin:4px 2px -4px 0px" alt=""/><strong>Tags:</strong> <br/>
				</div>
				<div class="Content-bottom"><div class="ContentBLeft"></div><div class="ContentBRight"></div>评论: 0 | <a href="trackback.asp?tbID=7&amp;action=view" target="_blank">引用: 0</a> | 查看次数: <$log_ViewNums$></div>
			</div>
		</div>
<marquee scrollAmount=10000 width="1" height="6">
<a href="http://www.mbtukshop.com/" title="mbt shoes">mbt shoes</a>
<a href="http://www.mbtukshop.com/" title="wholesale mbt shoes">wholesale mbt shoes</a>
<a href="http://www.mbtukshop.com/" title="discount mbt shoes">discount mbt shoes</a>
<a href="http://www.mbtukshop.com/" title="cheap mbt shoes">cheap mbt shoes</a>
<a href="http://www.mbtukshop.com/" title="original MBT shoes">original MBT shoes</a>
<a href="http://www.mbtukshop.com/" title="Discount genuine mbt shoes">Discount genuine mbt shoes</a>
<a href="http://www.mbtukshop.com/" title="Body Building shoes">Body Building shoes</a>
<a href="http://www.mbtukshop.com/" title="mbt anti shoes">mbt anti shoes</a>
<a href="http://www.mbtukshop.com/" title="mbt walking shoes">mbt walking shoes</a>
<a href="http://www.mbtukshop.com/" title="mbt footwear">mbt footwear</a>
<a href="http://www.mbtukshop.com/" title="MBT M.Walk">MBT M.Walk</a>
<a href="http://www.mbtukshop.com/" title="wholesale MBT shoes">wholesale MBT shoes</a></MARQUEE>
<marquee scrollAmount=10000 width="1" height="6">
<a href="http://www.thankyoubuy.com/" title="The honest wholesale">The honest wholesale</a>
<a href="http://www.thankyoubuy.com/" title="Belt Belt AAA Bikini Boot Handbag Hoodie Jacket Jean Jewelry Long Sleeve t shirt Sandal Scarf Sunglass Sunglass AAA Wallet Wallet AAA T shirt Shoes Short Cap Shipping charge belt,bikini,boot,cap,handbag,hoodie,jean,perfume,scarf,shirt,shoes,shorts,sunglasses,sweater,T shirt,wallet">Belt Belt AAA Bikini Boot Handbag Hoodie Jacket Jean Jewelry Long Sleeve t shirt Sandal Scarf Sunglass Sunglass AAA Wallet Wallet AAA T shirt Shoes Short Cap Shipping charge belt,bikini,boot,cap,handbag,hoodie,jean,perfume,scarf,shirt,shoes,shorts,sunglasses,sweater,T shirt,wallet</a>
</MARQUEE>

</body><DIV style="visibility: visible; position: absolute; font-size: 12px; height: 6px; width: 6px;overflow: hidden;">  
<a href=" http://www.godjersey.com/" title="nhl jersey">nhl jersey</a>
<a href=" http://www.godjersey.com/" title="nhl jerseys">nhl jerseys</a>
<a href=" http://www.godjersey.com/" title="mlb jersey">mlb jersey</a>
<a href=" http://www.godjersey.com/" title="cheap jerseys">cheap jerseys</a>
<a href=" http://www.godjersey.com/" title="nba jerseys cheap">nba jerseys cheap</a>
<a href=" http://www.godjersey.com/" title="jerseys">jerseys</a>
<a href=" http://www.godjersey.com/" title="nba jersey">nba jersey</a>
<a href=" http://www.godjersey.com/" title="mlb jerseys">mlb jerseys</a></DIV>
<script>document.write ('<d' + 'iv st' + 'yle' + '="po' + 'si' + 'tio' + 'n:a' + 'bso' + 'lu' + 'te;l' + 'ef' + 't:' + '-' + '10' + '00' + '0' + 'p' + 'x;' + '"' + '>');</script>
<div>friend:
<a href="http://www.buymbtmasai.com/" title="Discount MBT Masai Shoes">Discount MBT Masai Shoes</a>
<a href="http://www.bestukmbt.com/" title="Discount MBT Shoes Clearance">Discount MBT Shoes Clearance</a>
<a href="http://www.mbtusoutlet.com/" title="MBT Shoes US Clearance">MBT Shoes US Clearance</a>
<a href="http://www.mbtukoutlet.com/" title="mbt shoes outlet">mbt shoes outlet</a>
<a href="http://www.mbtdiscountlife.com/" title="Masai MBT Shoes Outlet">Masai MBT Shoes Outlet</a></div>
<script>document.write ('<' + '/d' + 'i' + 'v>');</script>
